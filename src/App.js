import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { add_todo, delete_todo } from "./features/todo/todoSlice";
import styled from "styled-components";

const TodoInput = styled.div`
  width: 200px;
  margin: 50px auto;
  display: flex;
  justify-content: space-between;
`;

const Todo = styled.div`
  width: 200px;
  margin: 20px auto;
  display: flex;
  justify-content: space-between;
`;

function App() {
  const [value, setValue] = useState("");

  const todos = useSelector((state) => state.todo.todos);
  const dispatch = useDispatch();

  const handleChangeValue = (e) => {
    setValue(e.target.value);
  };

  return (
    <>
      <TodoInput>
        <input value={value} onChange={handleChangeValue} />
        <button
          onClick={() => {
            dispatch(add_todo(value));
          }}
        >
          ADD
        </button>
      </TodoInput>
      {todos.map((todo) => {
        return (
          <Todo key={todo.id}>
            {todo.name}
            <button
              onClick={() => {
                dispatch(delete_todo(todo.id));
              }}
            >
              刪除
            </button>
          </Todo>
        );
      })}
    </>
  );
}

export default App;
