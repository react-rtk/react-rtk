import { createSlice } from "@reduxjs/toolkit";

let todoId = 0;

const initialState = {
  todos: [],
};

export const todoSlice = createSlice({
  name: "todo",
  initialState,
  reducers: {
    add_todo: (state, action) => {
      state.todos = [
        ...state.todos,
        {
          name: action.payload,
          id: todoId++,
        },
      ];
    },
    delete_todo: (state, action) => {
      state.todos = state.todos.filter((todo) => {
        return todo.id !== action.payload;
      });
    },
  },
});

// Action creators are generated for each case reducer function
export const { add_todo, delete_todo } = todoSlice.actions;

export default todoSlice.reducer;
